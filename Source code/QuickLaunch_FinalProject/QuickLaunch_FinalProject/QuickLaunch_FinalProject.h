#pragma once

#include "resource.h"
#include "Program.h"
#include <vector>
#include <string>
#include <iostream>
#include <shlwapi.h>
#include <windowsx.h>
#include <commctrl.h>
#include <shellapi.h>
#include <shlobj.h>
#include "shobjidl.h"
#include "shlguid.h"
#include "strsafe.h"
#include <math.h>
#include <algorithm>
#include <locale>
#include <codecvt>
#include <ObjIdl.h>
#include <gdiplus.h>

#pragma comment(lib, "shlwapi.lib")
#pragma comment(linker,"\"/manifestdependency:type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#pragma comment(lib, "ComCtl32.lib")
using namespace std;
using namespace Gdiplus;
#pragma comment (lib, "gdiplus.lib")

#define MAX_LOADSTRING 100
#define MAX_LEN_FILENAME 255

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name
WCHAR keyword[255];

RECT rcClient;
HWND g_Search;
HWND g_List;
HWND g_SearchBox;
HWND g_ListProgram;
vector<Program> g_Show;
vector<Program> g_Program;
NOTIFYICONDATA g_NotiInfoData;
HMENU g_hMenu;
GdiplusStartupInput gdiplusStartupInput;
ULONG_PTR           gdiplusToken;

ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	Statistics(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);

BOOL OnCreate(HWND hWnd, LPCREATESTRUCT lpCreateStruct);

//check exe file
bool isExe(TCHAR file[]);

//scan in program x86 folder
void scanFileX86();

//scan in program folder
void scanFile();

//search file exe
void searchFile(TCHAR* appPath, int level);

//init window
void InitWindow(HWND hWnd, int wndWidth, int wndHeight);

//load data to listview
void LoadData(vector<Program> data);

//function to search though find algorithm
int searchProgram(vector<Program> listProgram, wstring nameProgram);
//function to compare frequency of program
bool compare(Program p1, Program p2);

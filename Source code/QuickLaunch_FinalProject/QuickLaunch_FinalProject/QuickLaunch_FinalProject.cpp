// QuickLaunch_FinalProject.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "QuickLaunch_FinalProject.h"

int APIENTRY _tWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPTSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

 	// TODO: Place code here.
	MSG msg;
	HACCEL hAccelTable;

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_QUICKLAUNCH_FINALPROJECT, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance (hInstance, nCmdShow))
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_QUICKLAUNCH_FINALPROJECT));

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_QUICKLAUNCH_FINALPROJECT));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_BTNFACE + 1);
	wcex.lpszMenuName	= MAKEINTRESOURCE(IDC_QUICKLAUNCH_FINALPROJECT);
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   HWND hWnd;

   hInst = hInstance; // Store instance handle in our global variable

   hWnd = CreateWindowEx(0, szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
	   CW_USEDEFAULT, CW_USEDEFAULT, 500, 600, NULL, NULL, hInstance, NULL);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;

	switch (message)
	{
		HANDLE_MSG(hWnd, WM_CREATE, OnCreate);
	case WM_COMMAND:
		wmId = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		switch (wmId)
		{
		case ID_SEARCH:
			if (wmEvent == EN_CHANGE)
			{
				GetWindowText(g_SearchBox, keyword, 255);
				g_Show.clear();
				for (int i = 0; i < g_Program.size(); i++)
				{
					if (g_Program[i].getName().find(keyword) != -1)
					{
						g_Show.push_back(g_Program[i]);
					}
				}
				sort(g_Show.begin(), g_Show.end(), compare);
				LoadData(g_Show);
			}
			break;
		case IDM_SCAN:
			g_Program.clear();
			scanFile();
			scanFileX86();
			LoadData(g_Program);
			break;
		case IDM_STAT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_STATDIALOG), hWnd, Statistics);
			break;
			
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		}
		break;
	case WM_NOTIFY:
	{
		NMHDR* notiMes = (NMHDR*)lParam; //Notification Message
		LPNMTREEVIEW lpnmTree = (LPNMTREEVIEW)notiMes; //Contains information about a tree-view notification message;

		switch (notiMes->code)
		{
			case NM_DBLCLK:
			{
				if (notiMes->hwndFrom == g_ListProgram)
				{
					int i = ListView_GetSelectionMark(g_ListProgram);
					wchar_t* buffer = new wchar_t[255];
					ListView_GetItemText(g_ListProgram, i, 0, buffer, 255);
					for (int j = 0; j < g_Show.size(); j++)
					{
						if (g_Show[j].getName() == buffer)
						{
							ShellExecute(hWnd, L"open", g_Show[j].getPath().c_str(), NULL, NULL, SW_SHOW);
							g_Program[searchProgram(g_Program, g_Show[j].getName())].setFrequency(g_Program[j].getFrequency() + 1);
							break;
						}
					}
					delete[] buffer;
					sort(g_Program.begin(), g_Program.end(), compare);
					g_Show.clear();
					for (int i = 0; i < g_Program.size(); i++)
					{
						if (g_Program[i].getName().find(keyword) != -1)
						{
							g_Show.push_back(g_Program[i]);
						}
					}
					sort(g_Show.begin(), g_Show.end(), compare);
					LoadData(g_Show);
				}

				break;
				case NM_RETURN:
					if (notiMes->hwndFrom == g_ListProgram)
					{
						int i = ListView_GetSelectionMark(g_ListProgram);
						wchar_t* buffer = new wchar_t[255];
						ListView_GetItemText(g_ListProgram, i, 0, buffer, 255);
						for (int j = 0; j < g_Show.size(); j++)
						{
							if (g_Show[j].getName() == buffer)
							{
								ShellExecute(hWnd, L"open", g_Show[j].getPath().c_str(), NULL, NULL, SW_SHOW);
								g_Program[searchProgram(g_Program, g_Show[j].getName())].setFrequency(g_Program[j].getFrequency() + 1);
								break;
							}
						}
						delete[] buffer;
						sort(g_Program.begin(), g_Program.end(), compare);
						g_Show.clear();
						for (int i = 0; i < g_Program.size(); i++)
						{
							if (g_Program[i].getName().find(keyword) != -1)
							{
								g_Show.push_back(g_Program[i]);
							}
						}
						sort(g_Show.begin(), g_Show.end(), compare);
						LoadData(g_Show);
					}
				break;
			}
		}
	}
	case WM_QUICKLAUNCH:
	{
		wmId = LOWORD(lParam);
		switch (wmId)
		{
		case WM_LBUTTONDBLCLK:
			ShowWindow(hWnd, SW_SHOW);
			break;
		case WM_RBUTTONUP:
		{
			POINT pt;
			GetCursorPos(&pt);
			//only select menu items with left mouse
			TrackPopupMenu(g_hMenu, TPM_LEFTBUTTON, pt.x, pt.y, 0, hWnd, NULL);
			break;
		}
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
			break;
		}
		break;
	}
	case WM_HOTKEY:
		if (wParam == IDC_KEY)
		{
			ShowWindow(hWnd, SW_SHOWDEFAULT);
		}
		break;
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		// TODO: Add any drawing code here...
		EndPaint(hWnd, &ps);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	case WM_CLOSE:
		ShowWindow(hWnd, SW_HIDE);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}

BOOL OnCreate(HWND hWnd, LPCREATESTRUCT lpCreateStruct)
{
	GetClientRect(hWnd, &rcClient);
	int wndWidth = rcClient.right - rcClient.left;
	int wndHeight = rcClient.bottom - rcClient.top;
	InitWindow(hWnd, wndWidth, wndHeight);

	g_hMenu = CreatePopupMenu();
	InsertMenu(g_hMenu, 0, MF_BYPOSITION | MF_STRING, IDM_SCAN, L"Scan to build database");
	InsertMenu(g_hMenu, 1, MF_BYPOSITION | MF_STRING, IDM_STAT, L"Statistics");
	InsertMenu(g_hMenu, 2, MF_BYPOSITION | MF_STRING, IDM_EXIT, L"Exit");

	g_NotiInfoData.cbSize = sizeof(NOTIFYICONDATA);
	g_NotiInfoData.hWnd = hWnd;
	g_NotiInfoData.uID = IDI_QUICKLAUNCH;
	g_NotiInfoData.uVersion = NOTIFYICON_VERSION_4;
	g_NotiInfoData.uFlags = NIF_TIP | NIF_INFO | NIF_ICON | NIF_MESSAGE | NIF_SHOWTIP;
	g_NotiInfoData.uCallbackMessage = WM_QUICKLAUNCH;
	g_NotiInfoData.hIcon = LoadIcon(hInst, MAKEINTRESOURCE(IDI_QUICKLAUNCH_FINALPROJECT));

	// This text will be shown as the icon's tooltip.
	StringCchCopy(g_NotiInfoData.szTip, ARRAYSIZE(g_NotiInfoData.szTip), L"Quick Launch");
	StringCchCopy(g_NotiInfoData.szInfo, ARRAYSIZE(g_NotiInfoData.szInfo), L"Quick Launch");

	// Show the notification.
	Shell_NotifyIcon(NIM_ADD, &g_NotiInfoData);

	// Shift + Q
	RegisterHotKey(hWnd, IDC_KEY, MOD_SHIFT | MOD_NOREPEAT, 0x51);
	
	scanFileX86();
	scanFile();

	LoadData(g_Program);
	g_Show = g_Program;
	return true;
}

bool isExe(TCHAR file[])
{
	if (wcsstr(file, L".exe") != NULL)
	{
		return true;
	}
	return false;
}

void scanFileX86()
{
	//Init find_data struct
	WIN32_FIND_DATA  wfd; // Contains information about the file that is found by Find first file and Find next file
	TCHAR* programFile = new TCHAR[wcslen(L"C:\\Program Files (x86)") + 2];
	StrCpy(programFile, L"C:\\Program Files (x86)");
	StrCat(programFile, _T("\\*"));

	HANDLE hFile = FindFirstFileW(programFile, &wfd);
	bool found = true;

	//If the function fails or fails to locate files from the search string
	if (hFile == INVALID_HANDLE_VALUE) 
	{
		found = false;
	}
	while (found)
	{
		TCHAR* appPath;
		if ((wfd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) && (StrCmp(wfd.cFileName, _T(".")) != 0) && (StrCmp(wfd.cFileName, _T("..")) != 0))
		{
			appPath = new TCHAR[wcslen(L"C:\\Program Files (x86)") + wcslen(wfd.cFileName) + 2];
			StrCpy(appPath, L"C:\\Program Files (x86)");
			StrCat(appPath, L"\\");
			StrCat(appPath, wfd.cFileName);
			searchFile(appPath, 0);
		}
		found = FindNextFileW(hFile, &wfd);
	}
}

void scanFile()
{
	//Init find_data struct
	WIN32_FIND_DATA  wfd; //Contains information about the file that is found by Find first file and Find next file
	TCHAR* programFile = new TCHAR[wcslen(L"C:\\Program Files") + 2];
	StrCpy(programFile, L"C:\\Program Files");
	StrCat(programFile, _T("\\*"));

	HANDLE hFile = FindFirstFileW(programFile, &wfd);
	bool found = true;

	//If the function fails or fails to locate files from the search string
	if (hFile == INVALID_HANDLE_VALUE) 
	{
		found = false;
	}

	while (found)
	{
		TCHAR* appPath;
		if ((wfd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) && (StrCmp(wfd.cFileName, _T(".")) != 0) && (StrCmp(wfd.cFileName, _T("..")) != 0))
		{
			appPath = new TCHAR[wcslen(L"C:\\Program Files") + wcslen(wfd.cFileName) + 2];
			StrCpy(appPath, L"C:\\Program Files");
			StrCat(appPath, L"\\");
			StrCat(appPath, wfd.cFileName);

			searchFile(appPath, 0);
		}
		found = FindNextFileW(hFile, &wfd);
	}
}

void searchFile(TCHAR* appPath, int level)
{
	if (level >= 2)
	{
		return;
	}
	if (appPath == NULL || wcslen(appPath) == 0) 
	{
		return;
	}

	TCHAR* appContent = new TCHAR[wcslen(appPath) + 2];
	StrCpy(appContent, appPath);
	StrCat(appContent, L"\\*");

	WIN32_FIND_DATA wfd; // Contains information about the file that is found by Find first file and Find next file
	HANDLE hFile = FindFirstFileW(appContent, &wfd);
	bool found = true;
	if (hFile == INVALID_HANDLE_VALUE) 
	{
		found = false;
	}
	while (found) 
	{
		if (level == 1) 
		{
			if ((wfd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)) //Get only directory and folder
			{
				found = FindNextFileW(hFile, &wfd);
				continue;
			}
		}
		if ((StrCmp(wfd.cFileName, _T(".")) != 0) && (StrCmp(wfd.cFileName, _T("..")) != 0)) 
		{
			if (wfd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			{
				TCHAR* childDir;
				childDir = new TCHAR[wcslen(appPath) + wcslen(wfd.cFileName) + 2];
				StrCpy(childDir, appPath);
				StrCat(childDir, L"\\");
				StrCat(childDir, wfd.cFileName);
				searchFile(childDir, level + 1);

				delete[] childDir;
			}
			else {
				if (isExe(wfd.cFileName))
				{
					TCHAR* realPath;
					realPath = new TCHAR[wcslen(appPath) + wcslen(wfd.cFileName) + 2];
					StrCpy(realPath, appPath);
					StrCat(realPath, L"\\");
					StrCat(realPath, wfd.cFileName);

					Program p(wfd.cFileName, realPath);
					g_Program.push_back(p);
				}
			}
		}
		found = FindNextFileW(hFile, &wfd);
	}
}

void InitWindow(HWND hWnd, int wndWidth, int wndHeight)
{
	InitCommonControls();

	//Create font
	HFONT hFont;
	hFont= CreateFont(20, 0, 0, 0, FW_DONTCARE, FALSE, FALSE, FALSE, VIETNAMESE_CHARSET,
		OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, TEXT("Calibri"));

	g_Search = CreateWindowEx(0, L"Button", L"Search Program", WS_CHILD | WS_VISIBLE | BS_GROUPBOX,
		10, 10, 465, 70, hWnd, NULL, hInst, NULL);
	SendMessage(g_Search, WM_SETFONT, WPARAM(hFont), TRUE); // set font

	hFont = CreateFont(16, 0, 0, 0, FW_DONTCARE, FALSE, FALSE, FALSE, VIETNAMESE_CHARSET,
		OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, TEXT("Calibri"));

	g_SearchBox = CreateWindowEx(0, L"EDIT", L"", WS_CHILD | WS_VISIBLE | WS_BORDER, 25, 35, wndWidth - 50, 30, hWnd, (HMENU)ID_SEARCH, hInst, NULL);
	SendMessage(g_SearchBox, EM_SETCUEBANNER, 0, LPARAM(L"Type name of program to launch...")); //set placeholder
	SendMessage(g_SearchBox, WM_SETFONT, WPARAM(hFont), TRUE); // set font

	hFont = CreateFont(20, 0, 0, 0, FW_DONTCARE, FALSE, FALSE, FALSE, VIETNAMESE_CHARSET,
		OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, TEXT("Calibri"));

	g_List = CreateWindowEx(0, L"Button", L"List Program", WS_CHILD | WS_VISIBLE | BS_GROUPBOX,
		10, 90, 465, 435, hWnd, NULL, hInst, NULL);
	SendMessage(g_List, WM_SETFONT, WPARAM(hFont), TRUE); // set font

	hFont = CreateFont(16, 0, 0, 0, FW_DONTCARE, FALSE, FALSE, FALSE, VIETNAMESE_CHARSET,
		OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, TEXT("Calibri"));
	
	g_ListProgram = CreateWindowEx(LVS_EX_FULLROWSELECT, WC_LISTVIEW, L"",
		WS_CHILD | WS_VISIBLE | WS_BORDER | WS_VSCROLL | LVS_REPORT | LVS_EX_GRIDLINES,
		25, 120, wndWidth - 50, 390, hWnd, NULL, hInst, NULL);
	SendMessage(g_ListProgram, WM_SETFONT, WPARAM(hFont), TRUE); // set font
	SendMessage(g_ListProgram, LVM_SETEXTENDEDLISTVIEWSTYLE, 0, LVS_EX_FULLROWSELECT);

	//Init 2 columns
	LVCOLUMN lvCol;

	//Let the LVCOLUMN know that we will set the format, header text and width of it
	lvCol.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
	lvCol.fmt = LVCFMT_LEFT;

	lvCol.cx = 350;
	lvCol.pszText = L"Name";
	ListView_InsertColumn(g_ListProgram, 0, &lvCol);

	lvCol.fmt = LVCFMT_LEFT;
	lvCol.pszText = L"Frequency";
	lvCol.cx = wndWidth - 390;
	ListView_InsertColumn(g_ListProgram, 1, &lvCol);
}

void LoadData(vector<Program> data)
{
	ListView_DeleteAllItems(g_ListProgram);
	for (int i = 0; i < data.size(); i++)
	{
		//Add to ListView
		LV_ITEM lv;

		lv.mask = LVIF_TEXT | LVIF_PARAM | LVIF_IMAGE;

		//Insert type to first column
		lv.iItem = i;
		lv.iSubItem = 0;
		lv.pszText = new WCHAR[data[i].getName().size() + 1];
		wcscpy(lv.pszText, data[i].getName().c_str());
		ListView_InsertItem(g_ListProgram, &lv);

		lv.mask = LVIF_TEXT;
		lv.iSubItem = 1;
		lv.pszText = new WCHAR[to_wstring(data[i].getFrequency()).length() + 1];
		wcscpy(lv.pszText, to_wstring(data[i].getFrequency()).c_str());
		ListView_SetItem(g_ListProgram, &lv);
	}
}

INT_PTR CALLBACK Statistics(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
	{
		if (g_Program[0].getFrequency() == 0)
		{
			MessageBox(hDlg, L"No program has been run yet!", L"Error", MB_OK);
			EndDialog(hDlg, LOWORD(wParam));
		}
		else
		{
			GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);
		}
	}
		return (INT_PTR)TRUE;

	case WM_SYSCOMMAND:
		if (wParam == SC_CLOSE)
		{
			GdiplusShutdown(gdiplusToken);
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}

	case WM_PAINT:
	{
		PAINTSTRUCT ps;
		HDC hdc;
		hdc = BeginPaint(hDlg, &ps);

		vector<Color> color;
		color.push_back(Color(231, 76, 60));
		color.push_back(Color(52, 152, 21));
		color.push_back(Color(39, 174, 96));
		color.push_back(Color(142, 68, 173));
		color.push_back(Color(211, 84, 0));
		color.push_back(Color(44, 62, 80));

		int total = 0, others = 0;
		for (int i = 0; i < g_Program.size(); i++)
		{
			total += g_Program[i].getFrequency();
			if (i > 4)
			{
				others += g_Program[i].getFrequency();
			}
		}

		float startAngle = 0;
		float sweepAngle = 0;

		int origin_x = 125;
		int origin_y = 125;

		for (int i = 0; i < 6; i++)
		{
			if (i == 5)
			{
				sweepAngle = 360.0 * others / total;
				SetDCBrushColor(hdc, RGB(color[i].GetR(), color[i].GetG(), color[i].GetB()));
				SetDCPenColor(hdc, RGB(241, 241, 241));
				SelectObject(hdc, GetStockObject(DC_BRUSH));
				SelectObject(hdc, GetStockObject(DC_PEN));

				BeginPath(hdc);
				MoveToEx(hdc, origin_x, origin_y, NULL);
				AngleArc(hdc, origin_x, origin_y, 100, startAngle, sweepAngle);
				LineTo(hdc, origin_x, origin_y);
				EndPath(hdc);
				StrokeAndFillPath(hdc);
				startAngle += sweepAngle;
			}
			else
			{
				int freq = g_Program[i].getFrequency();
				if (freq > 0)
				{
					sweepAngle = 360.0 * freq / total;
					SetDCBrushColor(hdc, RGB(color[i].GetR(), color[i].GetG(), color[i].GetB()));
					SetDCPenColor(hdc, RGB(241, 241, 241));
					SelectObject(hdc, GetStockObject(DC_BRUSH));
					SelectObject(hdc, GetStockObject(DC_PEN));

					BeginPath(hdc);
					MoveToEx(hdc, origin_x, origin_y, NULL);
					AngleArc(hdc, origin_x, origin_y, 100, startAngle, sweepAngle);
					LineTo(hdc, origin_x, origin_y);
					EndPath(hdc);
					StrokeAndFillPath(hdc);
					startAngle += sweepAngle;
				}
			}
		}

		HFONT hFont = CreateFont(16, 0, 0, 0, FW_DONTCARE, FALSE, FALSE, FALSE, VIETNAMESE_CHARSET,
			OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, TEXT("Calibri"));
		SetBkMode(hdc, TRANSPARENT);
		SelectObject(hdc, hFont);
		SetTextColor(hdc, RGB(255, 255, 255));

		vector<vector<int>> coordNotes = {
			{ 325, 25 }, { 325, 125 }, { 325, 225 },
			{ 425, 25 }, { 425, 125 }, { 425, 225 }
		};
		for (int i = 0; i < 6; i++)
		{
			SetDCBrushColor(hdc, RGB(color[i].GetR(), color[i].GetG(), color[i].GetB()));
			Rectangle(hdc, coordNotes[i][0], coordNotes[i][1], coordNotes[i][0] + 50, coordNotes[i][1] + 50);
			RECT rect = { coordNotes[i][0], coordNotes[i][1], coordNotes[i][0] + 50, coordNotes[i][1] + 50 };
			float percent = 100.0 * g_Program[i].getFrequency() / total;
			if (i == 5)
			{
				percent = 100.0 * others / total;
			}
			percent = floor(percent);
			wstring per = to_wstring(percent).substr(0, 4);
			DrawText(hdc, (per + L" %").c_str(), -1, &rect, DT_SINGLELINE | DT_NOCLIP | DT_CENTER | DT_VCENTER);
		}

		SetTextColor(hdc, RGB(0, 0, 0));
		for (int i = 0; i < 6; ++i)
		{
			RECT rect = { coordNotes[i][0], coordNotes[i][1] + 70, coordNotes[i][0] + 120, coordNotes[i][1] + 50 };
			if (i == 5)
			{
				DrawText(hdc, L"Others", -1, &rect, DT_SINGLELINE | DT_NOCLIP | DT_LEFT | DT_VCENTER);
			}
			else
			{
				DrawText(hdc, g_Program[i].getName().c_str(), -1, &rect, DT_SINGLELINE | DT_NOCLIP | DT_LEFT | DT_VCENTER);
			}
		}

	}
		break;
	}

	return (INT_PTR)FALSE;
}

int searchProgram(vector<Program> listProgram, wstring nameProgram)
{
	Program p;
	p.setName(nameProgram);
	return distance(listProgram.begin(), find(listProgram.begin(), listProgram.end(), p));
}

bool compare(Program p1, Program p2)
{
	return p1.getFrequency() > p2.getFrequency();
}
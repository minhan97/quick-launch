//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by QuickLaunch_FinalProject.rc
//
#define IDC_MYICON                      2
#define IDD_QUICKLAUNCH_FINALPROJECT_DIALOG 102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_QUICKLAUNCH_FINALPROJECT    107
#define IDI_SMALL                       108
#define IDC_QUICKLAUNCH_FINALPROJECT    109
#define IDI_QUICKLAUNCH                 110
#define IDM_SCAN                        111
#define IDM_STAT                        112
#define WM_QUICKLAUNCH                  113
#define IDC_KEY                         114
#define IDL_LISTVIEW                    115
#define IDC_TEXTBOX                     117
#define IDC_LISTVIEW                    118
#define IDR_MAINFRAME                   128
#define IDD_STATDIALOG                  129
#define IDD_DIALOG2                     130
#define ID_SEARCH                       131
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif

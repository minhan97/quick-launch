#pragma once
#include <string>
using namespace std;

class Program
{
private:
	wstring Name;
	wstring Path;

	int Frequency;
public:
	Program();
	Program(wstring name, wstring path, int frequency = 0);
	~Program();

	wstring getName() const;
	wstring getPath();
	int getFrequency() const; 

	void setName(wstring name);
	void setPath(wstring path);
	void setFrequency(int frequency);

	bool operator==(const Program& p) const;

	bool operator>(const Program& p) const;
};


#include "stdafx.h"
#include "Program.h"


Program::Program()
{
	Name = L"";
	Path = L"";
	Frequency = 0;
}

Program::Program(wstring name, wstring path, int frequency)
{
	Name = name;
	Path = path;
	Frequency = frequency;
}

Program::~Program()
{
}

wstring Program::getName() const
{
	return Name;
}

wstring Program::getPath()
{
	return Path;
}

int Program::getFrequency() const
{
	return Frequency;
}

void Program::setName(wstring name)
{
	Name = name;
}

void Program::setPath(wstring path)
{
	Path = path;
}

void Program::setFrequency(int frequency)
{
	Frequency = frequency;
}

bool Program::operator==(const Program& p) const
{
	return Name == p.getName();
}

bool Program::operator>(const Program& p) const
{
	return Frequency > p.getFrequency();
}
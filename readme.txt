1. Thông tin
Lê Minh Ân
1512016

2.Các chức năng làm được
-Hiển thị các file exe trong thư mục Program Files và Program Files (x86) trong ổ đĩa C lên listview.
-Cho phép người dùng tìm kiếm file exe, sắp xếp theo thứ tự giảm dần số lần mở.
-Mở file thực thi bằng cách nhấn Enter hoặc double click vào tên file exe.
-Nhấn tổ hợp phím Shift + Q thì màn hình chính sẽ hiện ra.
-Vẽ biểu đồ tròn biểu thị số lần được mở của các chương trình (tối đa 5 chương trình có số lần được mở cao nhất và phần còn lại) và chú thích.

3. Link repo: https://minhan97@bitbucket.org/minhan97/quick-launch.git

4. Video demo: https://youtu.be/mOiq8fzc2t4